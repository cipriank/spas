﻿using System;
using System.Collections.Generic;

namespace SPas.Lex
{
    public struct Token
    {
        public string Text;
        public TokenKind Kind;
        public override string ToString()
        {
            return Text;
        }
    }

    public enum TokenKind
    {
        None, Spaces, Identifier, Operator, Reserved,
        IntLiteral, FloatLiteral,
        QuotedString,
        InlineCode
    }

    public class Lexer
    {
        static void SkipSpaces(string text, ref int pos)
        {
            int origPos;
            do {
                origPos = pos;
                pos += MatchPascalSpaces(text, pos);
                pos += MatchComment(text, pos);
            } while (origPos != pos);
        }

        private static int MatchComment(string text, int pos)
        {
            int matchLen = MatchStartEndText(text, pos, "{", "}");
            if (matchLen > 0)
                return matchLen;
            matchLen = MatchStartEndText(text, pos, "(*", "*)");
            if (matchLen > 0)
                return matchLen;
            matchLen = MatchStartEndText(text, pos, "//", "\n");
            if (matchLen > 0)
                return matchLen;
            return 0;
        }

        #region Base Matchers

        static int MatchStartEndText(string text, int pos, string start, string end)
        {
            if (text[pos] != start[0])
                return 0;
            if (text.Substring(pos, start.Length) != start)
                return 0;
            int endPos = text.Length - end.Length + 1;
            for (int i = pos + start.Length; i < endPos; i++)
            {
                if (text[i] != end[0])
                    continue;
                if (text.Substring(i, end.Length) == end)
                {
                    int result = i + end.Length - pos;
                    return result;
                }
            }
            return 0;
        }


        static int MatchCharFunction(string text, int pos, Func<char, bool> isMatchingChar)
        {
            for (int i = pos; i < text.Length; i++)
            {
                if (!isMatchingChar(text[i]))
                    return i - pos;
            }
            return text.Length - pos;
        }


        static int MatchCharFunction(string text, int pos, Func<char, bool> firstMatchingChar, Func<char, bool> isMatchingChar)
        {
            if (!firstMatchingChar(text[pos]))
            {
                return 0;
            }
            for (int i = pos + 1; i < text.Length; i++)
            {
                if (!isMatchingChar(text[i]))
                {
                    return i - pos;
                }
            }
            return text.Length - pos;
        }


        #endregion
        private static int MatchPascalSpaces(string text, int pos)
        {
            return MatchCharFunction(text, pos, char.IsWhiteSpace);
        }

        class TokenMatcher
        {
            public TokenKind Kind { get; }
            public Func<string, int, int> MatchingFunc { get; }

            public TokenMatcher(TokenKind kind, Func<string, int, int> matchingFunc)
            {
                Kind = kind;
                MatchingFunc = matchingFunc;
            }
        }

        static List<TokenMatcher> DefaultPageTagMatchers()
        {
            var result = new List<TokenMatcher>
            {
                new TokenMatcher(TokenKind.InlineCode, MatchInlineCode),
                new TokenMatcher(TokenKind.Reserved, MatchReserved),
                new TokenMatcher(TokenKind.QuotedString, MatchString),
                new TokenMatcher(TokenKind.Identifier, MatchIdentifier),
                new TokenMatcher(TokenKind.FloatLiteral, MatchFloatLiteral),
                new TokenMatcher(TokenKind.IntLiteral, MatchIntLiteral),
                new TokenMatcher(TokenKind.Operator, MatchOperator)
            };
            return result;
        }

        private static readonly List<TokenMatcher> PascalTokenMatchers = DefaultPageTagMatchers();
        private static readonly string[] Operators =
        {
            ":=",
            "@", "^",
            "=", "..", "[", "]",
            "<=", ">=", "<", ">",
            "+", "-", "*", "/",
            ":", ";", ".", ",", "(", ")"
        };

        static int MatchOperator(string text, int pos) {

            int remainingLength = text.Length - pos;
            foreach (var word in Operators)
            {
                if (word.Length > remainingLength)
                    continue;
                if (text[pos] != word[0])
                    continue;
                var subStr = text.Substring(pos, word.Length);
                if (subStr == word)
                    return word.Length;
            }
            return 0;
        }
private static int MatchFloatLiteral(string text, int pos)
        {
            var intMatcher = MatchIntLiteral(text, pos);
            if (0 == intMatcher)
                return 0;
            if (text[pos + intMatcher] != '.')
                return 0;
            var result = intMatcher + 1 + MatchIntLiteral(text, pos + intMatcher + 1);
            return result;
        }

        private static int MatchIntLiteral(string text, int pos)
        {
            return MatchCharFunction(text, pos, char.IsDigit);
        }

        private static int MatchString(string text, int pos)
        {
            if (text[pos] != '\'')
                return 0;
             int matchLen = MatchStartEndText(text, pos, "'", "'");
            return matchLen;
        }
        private static int MatchInlineCode(string text, int pos)
        {
            int matchLen = MatchStartEndText(text, pos, "@{", "}");
            return matchLen;
        }
        

        static int MatchIdentifier(string text, int pos)
        {
            return MatchCharFunction(text, pos, char.IsLetter, char.IsLetterOrDigit);
        }

        private static readonly string[] ReservedWords =
        {
            "and", "array", "as", "asm", "begin",
            "case", "class", "const", "constructor", "destructor",
            "dispinterface", "div", "do", "downto", "else",
            "end", "except", "exports", "file", "finalization",
            "finally", "for", "function", "goto", "if",
            "implementation", "in", "inherited", "initialization", "inline",
            "interface", "is", "label", "library", "mod",
            "nil", "not", "object", "of", "or",
            "out", "packed", "procedure", "program", "property",
            "raise", "record", "repeat", "resourcestring", "set",
            "shl", "shr", "string", "then", "threadvar",
            "to", "try", "type", "unit", "until",
            "uses", "var", "while", "with", "xor"
        };
    private static int MatchReserved(string text, int pos)
        {
            int lenMatch = MatchIdentifier(text, pos);
            if (lenMatch == 0)
                return 0;
            int lenMatchAlpha = MatchCharFunction(text, pos, char.IsLetter);
            if (lenMatchAlpha != lenMatch)
                return 0;
            string tokenText = text.Substring(pos, lenMatch);
            tokenText = tokenText.ToLower();
            foreach (var resWord in ReservedWords)
            {
                if (resWord.Length != lenMatch)
                    continue;
                if (tokenText == resWord)
                    return lenMatch;
            }
            return 0;
        }

        public static bool Advance(string text, ref int pos, ref Token token)
        {

            SkipSpaces(text,ref pos);

            foreach (var pascalMatcher in PascalTokenMatchers)
            {
                var len = pascalMatcher.MatchingFunc(text, pos);
                if (len == 0)
                {
                    continue;
                }
                token.Text = text.Substring(pos, len);
                token.Kind = pascalMatcher.Kind;
                pos += len;
                return true;
            }
            return false;
        }
    }
}