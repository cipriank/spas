﻿using System;
using System.Collections.Generic;

namespace SPas.Lex
{
    class LexContext
    {
        public Token Current;
        int _pos ;
        bool _eos;
        readonly string _text;

        private string RemainingText 
            => _text.Substring(_pos);


        public LexContext(string pasCode)

        {
            _text = pasCode;
            Next();
        }

        public Token Next()
        {

            if (_eos)
                return Current;
            _eos = !Lexer.Advance(_text, ref _pos, ref Current);
            return Current;
        }

        public Token Next(TokenKind expectedType)
        {
            if (_eos)
                return Current;
            _eos = !Lexer.Advance(_text, ref _pos, ref Current);
            Enforce.Eq(Current.Kind, expectedType);
            return Current;
        }
        public Token Next(string expectedText) 
        {
            if (_eos)
                return Current;
            _eos = !Lexer.Advance(_text, ref _pos, ref Current);
            Enforce.Eq(Current.Text, expectedText);
            return Current;
        }

        public List<Token> AdvanceUntil(string endToken)
        {
            return AdvanceUntil((tok) => tok.Text == endToken);
        }

        public List<Token> AdvanceUntil(Func<Token, bool> isEndToken)
        {

            var result = new List<Token>();
            result.Add(Current);
            while (!_eos)
            {
                Next();
                if (isEndToken(Current))
                    return result;
                result.Add(Current);
            }
            return result;
        }

        public ReservedWord.Kind GetReserved()
        {
            return this.Current.GetReservedWord();
        }
    }
}