﻿using System;
using System.IO;

namespace SPas.Lex
{
    internal static class Enforce
    {
        public static void Eq<T>(T expectedType, T actualValue)
        {
            if(!expectedType.Equals(actualValue))
                throw new InvalidDataException("Expected "+expectedType + " but got: "+actualValue);
        }

        public static void InvalidBranch()
        {
            throw new InvalidOperationException("Invalid branch");
        }
    }
}