﻿namespace SPas.Lex
{
    static class ReservedWord
    {
        internal enum Kind
        {
            None,
            Unit, Interface, Implementation,
            Begin, End,
            Uses,
            Const,
            Function,
            Procedure,
        };
        static readonly (string text, Kind kind)[] ReservedWordTokens =
        {
            ("begin", Kind.Begin),
            ("end", Kind.End),
            ("const", Kind.Const),
            ("interface", Kind.Interface),
            ("implementation", Kind.Implementation),
            ("function", Kind.Function),
            ("procedure", Kind.Procedure),
            ("uses", Kind.Uses),
            ("unit", Kind.Unit),
        };

        public static Kind GetReservedWord(this Token word)
        {
            var lowWord = word.Text.ToLower();
            foreach (var item in ReservedWordTokens)
            {
                if (item.text == lowWord)
                    return item.kind;
            }

            return Kind.None;
        }

    }
}