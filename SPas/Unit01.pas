unit Unit01;

interface

function addOne(v: Integer): Integer;

implementation

function addOne(v: Integer): Integer;
begin
  Result:= v+1;
end;

end.