﻿using System;
using System.IO;
using SPas.Lex;
using SPas.Parse;
using SPas.Semantic;

namespace SPas
{
    class Program
    {
        static void Main(string[] args)
        {
            var sysCode = File.ReadAllText("system.pas");

            var systemUnit = Parser.ParsePascalFile(sysCode);

            var semProg = new SemanticProgram();
            
            var code = File.ReadAllText("prog01.pas");
            var progNode = Parser.ParsePascalFile(code);
            semProg.AddProgram((ProgramNode)progNode);
            Console.WriteLine("Node: "+progNode.Kind);
        }
    }
}
