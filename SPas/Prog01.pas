﻿uses unit1;
Const
	c = '4';        { Character type constant. }
	e = 2.7182818;  { Real type constant. }
	a = 2;          { Ordinal (Integer) type constant. }
	s = 'This is a constant string'; {String type constant.}
	P = Nil;

begin
	writeln(addOne(2)+addOne(3));
end.
