using System;
using System.Collections.Generic;

namespace SPas.Semantic
{
    
    public class SemUnit
    {
        public string Name { get; set; }
        public Dictionary<string, SemUnit> InterfaceUses { get;  } = new Dictionary<string, SemUnit>();
        public Dictionary<string, SemUnit> ImplementationUses { get;  } = new Dictionary<string, SemUnit>();
        public ConstTable ConstValues { get; } = new ConstTable();

    }

    public class ConstTable
    {
        public Dictionary<string, ConstValue> Values { get; } = new Dictionary<string, ConstValue>();
    }

    public class ConstValue
    {
        public object Value;
        public TypeCode Kind;
    }
}