using System;
using System.Collections.Generic;
using SPas.Lex;
using SPas.Parse;
using SPas.Parse.Expressions;
using SPas.Parse.Nodes;

namespace SPas.Semantic
{
    class SemanticConstSolver
    {
        private List<SemUnit> UsedUnits { get; } = new List<SemUnit>();

        public void Solve(ConstTable constTable, ConstItem item)
        {
            constTable.Values[item.Name] = EvaluateConst(item.Items);
        }

        private ConstValue EvaluateConst(List<Token> itemItems)
        {
            if (itemItems.Count == 1)
            {
                return EvaluateConstToken(itemItems[0]);
            }
            var expTree = ExpressionParser.ParseExpression(itemItems);
            return null;
        }

        private ConstValue EvaluateConstToken(Token itemItem)
        {
            var result = new ConstValue();
            
            switch (itemItem.Kind)
            {
                case TokenKind.IntLiteral:
                    result.Value = int.Parse(itemItem.Text);
                    break;
                case TokenKind.FloatLiteral:
                    result.Value = double.Parse(itemItem.Text);
                    break;
                case TokenKind.QuotedString:
                    result.Value = (itemItem.Text);
                    break;
                
                case TokenKind.Reserved:
                    Enforce.Eq("nil", itemItem.Text.ToLower());
                    result.Value = null;
                    break;
                default:
                    Enforce.InvalidBranch();
                    break;
                    
                    
            }

            return result;
        }
    }
    class SemanticProgram
    {
        public Dictionary<string, SemUnit> UsedUnits = new Dictionary<string, SemUnit>();
        public ConstTable ConstValues { get; } = new ConstTable();

        public void AddProgram(ProgramNode progNode)
        {
            foreach (var nodeItem in progNode.Items)
            {
                if (nodeItem is ConstClause constClause)
                {
                    EvaluateConstClause(constClause);
                }
                else
                {
                    
                }
            }
        }

        private void EvaluateConstClause(ConstClause constClause)
        {
            SemanticConstSolver semantic = new SemanticConstSolver();
            foreach (var clauseItem in constClause.Items)
            {
                semantic.Solve(ConstValues, clauseItem);
            }
        }
    }
}