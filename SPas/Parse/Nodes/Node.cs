using System.Collections;
using System.Collections.Generic;
using SPas.Lex;

namespace SPas.Parse.Nodes
{
    public enum NodeKind
    {
        None,
        Program,
        Unit,
        UnitInterface,
        UnitImplementation,
        Function,
        FunctionImplementation,
        Uses,
        Const,
        ConstItem,
        BeginEnd,
        Statement,
        IdentifierTerminal,
        LiteralValue,
        CallNode,
        Argument,
        BinaryExpression
    };
    public class Node
    {
        public NodeKind Kind;

        public Node(NodeKind kind)
        {
            Kind = kind;
        }
    }

    public class NamedDeclaration : Node
    {
        public string Name;
        public override string ToString()
        {
            return Name;
        }

        public NamedDeclaration(NodeKind kind) : base(kind)
        {
        }
    }

    public class ItemsNode<T> : Node, IEnumerable<T>
    {
        public ItemsNode(NodeKind kind) : base(kind)
        {
        }
        public List<T> Items { get; } = new List<T>();
        public IEnumerator<T> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) Items).GetEnumerator();
        }
    }

    public class NamedItemsNode<T> : ItemsNode<T>
    {
        public string Name;
        public NamedItemsNode(NodeKind kind) : base(kind)
        {
        }
    }



    internal class UnitInterface: ItemsNode<Node>
    {
        public UsesClause Uses { get; } = new UsesClause();
        public UnitInterface() : base(NodeKind.UnitInterface)
        {
        }
    }


    public class StatementNode : Node
    {
        public Node Expression;
        public StatementNode(NodeKind kind) : base(kind)
        {
        }
    }

    class Argument : NamedDeclaration
    {
        public string Modifier = "";
        public List<Token> TypeTokens { get; } = new List<Token>();

        public Argument() : base(NodeKind.Argument)
        {
        }
    }
}