using SPas.Lex;

namespace SPas.Parse.Nodes
{
    class ConstItem : NamedItemsNode<Token>
    {
        public ConstItem() : base(NodeKind.ConstItem)
        {
        }
    }
    class ConstClause : ItemsNode<ConstItem>
    {
        public ConstClause() : base(NodeKind.Const)
        {
        }
    }
}