using SPas.Parse.Nodes;

namespace SPas.Parse
{
    class UnitNode : NamedDeclaration
    {
        public UnitNode() : base(NodeKind.Unit)
        {
        }

        public UnitInterface Interface { get; } = new UnitInterface();
        public UnitImplementation Implementation { get; } = new UnitImplementation();
    }
}