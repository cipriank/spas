using SPas.Parse.Nodes;

namespace SPas.Parse
{
    internal class FunctionImplementation : FunctionDeclaration
    {
        public FunctionImplementation()
        {
            Kind = NodeKind.FunctionImplementation;
        }
    }
}