using SPas.Lex;
using SPas.Parse.Expressions;
using SPas.Parse.Nodes;

namespace SPas.Parse
{
    class BeginEndBlock : ItemsNode<StatementNode>
    {
        public BeginEndBlock() : base(NodeKind.BeginEnd)
        {
        }

        public void Parse(LexContext context)
        {context.Next();
             while (true)
             {
                 var reserved = context.GetReserved();
                 switch (reserved)
                 {
                 case ReservedWord.Kind.None:
                     ParseStatement(context);
                     break;
                 case ReservedWord.Kind.End:
                     return;
                 default:
                     Enforce.InvalidBranch();
                     return;
                 }
         
             }
        }

        static bool IsEndOfStatement(Token tok)
        {
            if (tok.Text == ";")
                return true;
            if (tok.GetReservedWord()==ReservedWord.Kind.End)
                return true;
            return false;
        }
        private void ParseStatement(LexContext context)
        {
            var  tokens = context.AdvanceUntil(IsEndOfStatement);
            if (context.Current.Text == ";")
                context.Next();
            Items.Add(ExpressionParser.Parse(tokens));
        }
    }
}