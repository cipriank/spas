using System.Collections.Generic;
using SPas.Lex;
using SPas.Parse.Nodes;

namespace SPas.Parse
{
    class FunctionDeclaration : NamedDeclaration
    {
        public List< Argument> Arguments { get; } = new List<Argument>();
        public TypeDeclaration ReturnType { get; set; }
        public FunctionDeclaration() : base(NodeKind.Function)
        {
        }

        public void ParseDeclaration(LexContext context, bool isProcedure)
        { 
            Name = context.Next().Text;
            var tok = context.Next();
            if(tok.Text == "(")
            {
                //handle parameters
                while (true) 
                {
                    tok = context.Next();
                    if(tok.Text == ")")
                        break;
                    var argument = new Argument
                    {
                        Name = tok.Text
                    };
                    context.Next(":");
                    tok = context.Next();
                        argument.TypeTokens.Add(tok);
         
                    this.Arguments.Add(argument);
         
                }
            }

            if (!isProcedure)
            {
                context.Next(":");
                tok = context.Next();
                this.ReturnType = TypeDeclaration.Resolve(tok.Text);
                context.Next();
            }
            else
            {
                context.Next(";");
            }

            context.Next();
        }

    }
}