using SPas.Parse.Nodes;

namespace SPas.Parse
{
    public class UsesClause : ItemsNode<string>
    {
        public UsesClause() : base(NodeKind.Uses)
        {
        }
    }
}