using SPas.Parse.Nodes;

namespace SPas.Parse
{
    internal class UnitImplementation : ItemsNode<Node>
    {
        public UsesClause Uses { get; } = new UsesClause();
        public UnitImplementation() : base(NodeKind.UnitImplementation)
        {
        }
    }
    internal class ProgramNode : NamedItemsNode<Node>
    {
        public UsesClause Uses { get; set; } 
        public BeginEndBlock BeginEnd { get; } = new BeginEndBlock();
        public ProgramNode() : base(NodeKind.Program)
        {
        }
    }
}