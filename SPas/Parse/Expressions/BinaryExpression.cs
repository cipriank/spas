using System.Collections.Generic;
using System.Linq;
using SPas.Parse.Nodes;

namespace SPas.Parse.Expressions
{
    class BinaryExpression : Node
    {
        public Node Left;
        public Node Right;
        public string Op;

        public BinaryExpression() : base(NodeKind.BinaryExpression)
        {
        }

        private static readonly string[] MathOperators = { "+", "-", "*",  "/", 
            "div", "mod",
            "and", "or"
        };

        public static bool IsMathOperator(string tokenText)
        {
            return MathOperators.Contains(tokenText);
        }

        public override string ToString()
        {
            return Left + " " + Op + " " + Right;
        }

        public static BinaryExpression FoldExpression(List<UnresolvedNode> tokens, int opPos)
        {
            var leftItems = tokens.Take(opPos).ToList();
            var right = tokens.Skip(opPos + 1).ToList();
            var binaryExpression = new BinaryExpression
            {
                Op = tokens[1].Text,
                Left = ExpressionParser.ResolveAndFoldTokens(leftItems),
                Right = ExpressionParser.ResolveAndFoldTokens(right)
            };
            return binaryExpression;
        }
    }
}