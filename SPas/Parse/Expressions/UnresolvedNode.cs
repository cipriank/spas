using SPas.Lex;
using SPas.Parse.Nodes;

namespace SPas.Parse.Expressions
{
    public class UnresolvedNode : Node
    {
        public Token Value;
        public Node SolvedNode;
        public TokenKind TokKind 
            => Value.Kind;
        public string Text 
            => Value.Text;

        public override string ToString()
        {
            if (SolvedNode != null)
                return SolvedNode.ToString();

            return Text;
        }

        public UnresolvedNode(Token value) : base(NodeKind.None)
        {
            Value = value;
        }

        public UnresolvedNode(Node solvedNode) : base(solvedNode.Kind)
        {
            Value.Kind = TokenKind.None;
            SolvedNode = solvedNode;
        }
    }
}