using System.Collections.Generic;
using System.Linq;
using SPas.Lex;
using SPas.Parse.Nodes;

namespace SPas.Parse.Expressions
{

    class TerminalIdentifier : NamedDeclaration
    {
        public TerminalIdentifier() : base(NodeKind.IdentifierTerminal)
        {
        }
    }
    class TerminalLiteralValue : Node
    {
        public string Value;
        public TerminalLiteralValue() : base(NodeKind.LiteralValue)
        {
        }

        public override string ToString()
        {
            return Value;
        }
    }

    public static class ExpressionParser
    {
        public static Node ParseExpression(List<Token> tokens)
        {
            return ResolveAndFoldTokens(tokens.Select(tok => new UnresolvedNode(tok)).ToList());
        }

        static bool FindingFoldCall(List<UnresolvedNode> tokens)
        {
            if(tokens.Count<=2)
                return false;
            var result = false;
            for (int i = 0; i < tokens.Count - 1; i++)
            {
                if (tokens[i].TokKind != TokenKind.Identifier || tokens[i + 1].Text != "(") continue;
                var resultFold = CallNode.FoldCall(tokens, i+1);

                result = true;
            }

            return result;
        }
        
        public static Node ResolveAndFoldTokens(List<UnresolvedNode> tokens)
        {
            if (tokens.Count == 1)
            {
                switch (tokens[0].TokKind)
                {
                    case TokenKind.Identifier:
                        return new TerminalIdentifier()
                        {
                            Name = tokens[0].Text
                        };
                    case TokenKind.IntLiteral:
                    case TokenKind.FloatLiteral:
                    case TokenKind.QuotedString:
                        return new TerminalLiteralValue()
                        {
                            Value = tokens[0].Text
                        };
                    case TokenKind.None:
                        return tokens[0].SolvedNode;
                    default:
                        Enforce.InvalidBranch();
                        break;
                }
            }

            if (FindingFoldCall(tokens))
            {
                return ResolveAndFoldTokens(tokens);
            }

            if (tokens.Count == 3 && BinaryExpression.IsMathOperator(tokens[1].Text))
            {
                return BinaryExpression.FoldExpression(tokens, 1);
            }
            

            Enforce.InvalidBranch();
            return null;
        }


        public static StatementNode Parse(List<Token> tokens)
        {
            StatementNode statementNode = new StatementNode(NodeKind.Statement);
            statementNode.Expression = ParseExpression(tokens);
            return statementNode;
        }
    }
}