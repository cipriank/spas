using System.Collections.Generic;
using System.Linq;
using SPas.Parse.Nodes;

namespace SPas.Parse.Expressions
{
    class CallNode : Node
    {
        public Node Callee;
        public List<Node> CallArguments { get; }= new List<Node>();
        public CallNode() : base(NodeKind.CallNode)
        {
        }

        public override string ToString()
        {
            return $"{Callee}({string.Join(", ", CallArguments)})";
        }
        
        static int NextMatchingParam(List<UnresolvedNode> tokens, int startPos)
        {
            int openParen = 0;
            for (int i = startPos; i < tokens.Count; i++)
            {
                if (tokens[i].Text == ")")
                {
                    if (openParen == 0)
                    {
                        return i - startPos;
                    }
                    
                    openParen--;
                }
                if (tokens[i].Text == "(")
                {
                    openParen++;
                }
            }

            return 0;
        }

        public static (CallNode node, int closeParen) FoldCall(List<UnresolvedNode> tokens, int startParenPos)
        {
            var startPos = startParenPos;
            var result = new CallNode();
            List<UnresolvedNode> leftRange = new List<UnresolvedNode>() {tokens[startPos-1] };
            result.Callee = ExpressionParser.ResolveAndFoldTokens(leftRange);
            int nextParamLen;
            do
            {
                nextParamLen = NextMatchingParam(tokens, startPos + 1);
                var argTokens = tokens.Skip(startPos + 1).Take(nextParamLen).ToList();
                var argExpr = ExpressionParser.ResolveAndFoldTokens(argTokens);
                startPos += nextParamLen + 1;
                result.CallArguments.Add(argExpr);
                
            } while (tokens[startPos].Text != ")");

            var closeParenPos = startPos;
            var rangeCall = tokens.Skip(startParenPos).Take(closeParenPos +1).ToList();
            var beforeRange = tokens.Take(startParenPos - 1).ToList();
            var afterRange = tokens.Skip(startParenPos+closeParenPos).ToList();
            tokens.Clear();
            tokens.AddRange(beforeRange);
            tokens.Add(new UnresolvedNode(result));
            tokens.AddRange(afterRange);

            return (result, startPos);
        }
    }
}