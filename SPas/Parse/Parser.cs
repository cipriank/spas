using System.Linq;
using SPas.Lex;
using SPas.Parse.Nodes;

namespace SPas.Parse
{
    class Parser
    {
        public static Node ParsePascalFile(string fileCode)
        {
            LexContext context = new LexContext(fileCode);

            var reserved = context.GetReserved();

            if (reserved == Lex.ReservedWord.Kind.Unit)
            {
                return ParseUnit(context);
            }

            return ParseProgram(context);
        }

        private static Node ParseProgram(LexContext context)
        { 
            var result = new ProgramNode();
            while (true)
            {
                var reserved = context.GetReserved();

                switch (reserved)
                {
                    case ReservedWord.Kind.Uses:
                    {
                        result.Uses = new UsesClause();
                        ParseUses(result.Uses, context);
                        break;
                    }
                    case ReservedWord.Kind.Const:
                    {
                        var consts = new ConstClause();
                        ParseConsts(consts, context);
                        result.Items.Add(consts);
                        break;
                    }

                    case ReservedWord.Kind.Begin:
                    {
                        result.BeginEnd.Parse(context);
                        context.Next(".");
                        return result;
                    }
                    default:
                        Enforce.InvalidBranch();
                        break;
                }
            }
        }

        private static void ParseConsts(ConstClause constClause, LexContext context)
        { context.Next();
                 do
                 {
                     var tokens = context.AdvanceUntil(";");
                     var constItem = new ConstItem
                     {
                         Name = tokens[0].Text
                     };
                     constItem.Items.AddRange(tokens.Skip(2));
                     constClause.Items.Add(constItem);
                     //TODO: Do not drop Const tokens
                     context.Next();
                 }
                 while (context.GetReserved() == ReservedWord.Kind.None);
        }

        private static void ParseUses(UsesClause usesClause, LexContext context)
        {
            context.Next();
            var tokens = context.AdvanceUntil(";");
            usesClause.Items.Add(tokens[0].Text);
            context.Next();
        }

        private static NamedDeclaration ParseUnit(LexContext context)
        {
            var result = new UnitNode
            {
                Name = context.Next(TokenKind.Identifier).Text
            };

            context.Next(";");
            context.Next();

            ParseInterface(context, result);
            ParseImplementation(context, result);
            return result;
        }

        private static void ParseImplementation(LexContext context, UnitNode unit)
        {
            var fun = new FunctionImplementation();
        }

        private static void ParseInterface(LexContext context, UnitNode unit)
        {
            context.Next();
            while (true)
            {
                var reserved = context.GetReserved();
                switch (reserved)
                {
                    case ReservedWord.Kind.Implementation:
                        return;
                    case ReservedWord.Kind.Function:
                        ParseFunctionInInterface(context, false);
                        break;
                    case ReservedWord.Kind.Procedure:
                        ParseFunctionInInterface(context, true);
                        break;
                    default:
                        Enforce.InvalidBranch();
                        break;
                }
            }
        }

        private static void ParseFunctionInInterface(LexContext context, bool isProcedure)
        {
            var funDecl = new FunctionDeclaration();
            funDecl.ParseDeclaration(context, isProcedure);
        }
    }
}