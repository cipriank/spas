﻿using NUnit.Framework;
using SPas.Lex;

namespace SPas.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            int pos = 0;
            var token = new Token();
            var code = "@{#include<iostream>}";
            Lexer.Advance(code, ref pos, ref token);
            Assert.AreEqual(code, token.Text);
        }
    }
}